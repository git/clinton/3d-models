/* Customizable Clip for mounting strip LEDs
   Copyright (c) 2015 Clinton Ebadi <clinton@unknownlamer.org>

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see
   <http://www.gnu.org/licenses/>.
*/

// TODO:
//  - make this a module

use <MCAD/teardrop.scad>

// All lengths in mm

wall_height = 4; // How tall should the wall be?
wall_width = 3.2; // How thick should the walls be at their base?
extra_wall_height = 10; // How tall should the horizontal mounting tab be? 0 = no tab.

base_height = 2.5; // Thickness of the base
base_length = 30; // Length of the clip

strip_width  = 10; // Width of LED strip

hole_radius = 2; // Size of mounting holes

x_midpoint = (wall_width*2 + strip_width) / 2;

difference () {
     rotate([90,0,0]) {
	  linear_extrude(height = base_length) {
	       translate ([0, wall_height + base_height, 0]) square([wall_width, extra_wall_height]);
	       difference () {
		    square ([wall_width * 2 + strip_width, wall_height + base_height]);
		    polygon (points = [[wall_width, base_height], [wall_width + strip_width, base_height],
				       [(wall_width*2 + strip_width) / 2, (wall_height + base_height) * 3]],
			     paths = [[0, 1, 2]]);
	       }
	  }

     }
     translate ([x_midpoint, -base_length / 2, -0.1]) cylinder (h = base_height + 0.2, r = hole_radius, $fn = 20);
     translate([0.1, -base_length/2, (base_height + wall_height + extra_wall_height) - hole_radius*2 ]) teardrop (radius = hole_radius, length = wall_width*2 + 0.2, angle = 90);
     
}
	
