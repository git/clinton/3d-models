use <threads.scad>

// pretty sure this is wrong, it definitely doesn't fit a tap handle...

difference () {
     %cylinder (r=10, h = 40, $fn = 120);
     translate ([0, 0, -0.5]) english_thread (diameter=3/8, threads_per_inch=16, length=9/8, internal = true);
}
