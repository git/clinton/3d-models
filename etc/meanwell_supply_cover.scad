// Power Supply Cover
// Copyright (c) 2014 "gregington"
// CC BY-SA 3.0, http://www.thingiverse.com/thing:216951

// Customized by Clinton Ebadi <clinton@unknownlamer.org> for the
// SE-600 power supply. Copyright (c) 2016.

// TODO:

//  - powerSocket, powerSwitch, screwHoles, ... might be cleaner if
//    placement were done separately. Should be possible to add
//    modules for placing parts (lying flat, centered on the origin)
//    on each surface of the psu cover, fine tuning positions using
//    translate().
//
//  - add powerpole mount option

// new todo: - recheck dimensions -- screw holes are fixed offset from
// cover_depth... cover_depth needs to be rechecked/recalculated as
// wall + clearance needed for power socket + screw y offset

// or: optimal case depth is buffer + screw hole distance from edge of
// case + whatever space is needed inside the case + back wall
// thickness


// case depth = back wall thickness + cavity size + mounting hole offset + extra behind mounting hole

// cavity size is usually just enough for the plug or terminals + some extra

WALL = 2;

PSU_CLEARANCE = 1;
PSU_WIDTH = 127 + PSU_CLEARANCE;
PSU_HEIGHT = 63.5 + PSU_CLEARANCE;
PSU_DEPTH = 200;

COVER_WIDTH = PSU_WIDTH + (2 * WALL);
COVER_HEIGHT = PSU_HEIGHT + (2 * WALL);

COVER_DEPTH = 11 + 6 + 2*(25.4+2) + WALL; // ac
//COVER_DEPTH = 11 + 6 + 30 + WALL; // dc

//COVER_SUPPORT_WIDTH = 20; // ??

VENT_HEIGHT = PSU_HEIGHT - PSU_CLEARANCE - WALL - 1;
VENT_WIDTH = 2;
VENT_DISTANCE = 5; // was 4

CABLE_SLOT_WIDTH = 18;
CABLE_SLOT_HEIGHT = 50;
CABLE_SLOT_Y_OFFSET = 40;

//CABLE_SLOT_VENT_DISTANCE = 2; // ???

POWER_SOCKET_OFFSET = WALL*2 + 43/2;
POWER_SWITCH_OFFSET = 20;

SCREW_HOLE_HEIGHT = 12;
SCREW_HOLE_WIDTH = 3;
SCREW_HOLE_Z_OFFSET = 30; // offset from top of PSU
SCREW_HOLE_GAP = 20; // distance between screw holes
SCREW_HOLE_Y_OFFSET = COVER_DEPTH - 6; // offset from back of case


module cover() {
  difference() {
       minkowski () {
	    cube([COVER_WIDTH, COVER_DEPTH, COVER_HEIGHT]);
//	    sphere (d=1);
       }
	    translate([WALL, WALL, WALL]) {
#		 cube([PSU_WIDTH, PSU_DEPTH, PSU_HEIGHT]);
	    }
  }
}

module vents(offset = 0, limit = COVER_WIDTH - WALL) {
  for (x = [ WALL + VENT_DISTANCE + offset : VENT_DISTANCE : limit ]) {
    translate([x, WALL, (COVER_HEIGHT - VENT_HEIGHT) / 2]) {
      rotate([0, -90, 90]) {
	   slot(VENT_WIDTH, VENT_HEIGHT, 2 * WALL);
      }
    }
  }

}

module slot(width, height, depth, center = false) {
  translate([width / 2, 0, 0]) {
    union() {
      cube([height - width, width, depth]);
      translate([0, width / 2, 0]) {
        cylinder(h = depth, r = width / 2);
      }
      translate([height - width, width / 2, 0]) {
        cylinder(h = depth, r = width / 2);
      }
    }
  }
}

module cslot(width, height, depth) {
     translate ([-height/2, -width/2, -depth/2])
     slot (width, height, depth);
}


// defaults for https://www.digikey.com/product-detail/en/1-1609112-2/CCM1916-ND/2292077
//module powerSocket(width = 27, height = 38, screw_distance = 35, side=0) {
module powerSocket(width = 1.06*25.4, height = 1.56*25.4, screw_distance = 1.42*25.4, side=0) {
  translate([WALL*(side ? 2 : 1) + 2 + (side ? PSU_WIDTH : 0), POWER_SOCKET_OFFSET, (COVER_HEIGHT/2+height)/2]) {
    rotate([0, 90, 0]) {
      translate([0,screw_distance/2,-5]) { rotate ([0, 0, 90]) cslot (3, 5, 22); }
      translate([0,-screw_distance/2,-5]) { rotate ([0, 0, 90]) cslot (3, 5, 22); }
      translate([0,0,-5.5 - 1.5])
        minkowski() {
          #cube([height-8,width-8,11], center=true);
          cylinder(r=4.5,h=0.1);
      }
    }
  }
}

module powerSwitch() {
  translate([COVER_WIDTH / 2, POWER_SWITCH_OFFSET, COVER_HEIGHT + 2]) {
    rotate([0, 0, -90]) {
	  cube([14.8,20.8,2], center=true);
	  translate([0,0,-7-1]) cube([13,19.8,14], center=true);
    }
  }
}

module cableSlot() {
  translate([(PSU_WIDTH - CABLE_SLOT_HEIGHT) / 2, WALL, (COVER_HEIGHT - VENT_HEIGHT) / 2]) {
    rotate([90, 0, 0]) {
      slot(CABLE_SLOT_WIDTH, CABLE_SLOT_HEIGHT, WALL * 2);
    }
  }
}

module cableSlotOutline() {
  border = 4;
  translate([(PSU_WIDTH - CABLE_SLOT_HEIGHT - border) / 2, WALL, (COVER_HEIGHT - VENT_HEIGHT - border) / 2]) {
    rotate([90, 0, 0]) {
      slot(CABLE_SLOT_WIDTH + border, CABLE_SLOT_HEIGHT + border, WALL * 2);
    }
  }
}

module screwHoles() {
     translate ([-2.5, SCREW_HOLE_Y_OFFSET + WALL - SCREW_HOLE_HEIGHT, -SCREW_HOLE_WIDTH/2 + WALL + + PSU_CLEARANCE/2 + SCREW_HOLE_Z_OFFSET]) {
    rotate([90, 0, 90]) {
	 #slot(SCREW_HOLE_WIDTH, SCREW_HOLE_HEIGHT, COVER_WIDTH + 5);
	 translate ([0, SCREW_HOLE_GAP, 0])
	      slot(SCREW_HOLE_WIDTH, SCREW_HOLE_HEIGHT, COVER_WIDTH + 5);
    }
  }
}

module ac_cover () {
     translate([0, COVER_HEIGHT, 0]) rotate([90, 0, 0]) {
	  difference() {
	       cover();
	       vents(offset = PSU_WIDTH/2 );
	       powerSocket(side=1);
	       screwHoles();
	  }
     }
}

module dc_cover () {
     translate([0, COVER_HEIGHT, 0]) rotate([90, 0, 0]) {
	  difference() {
	       cover();
	       difference () {
		    vents(limit = PSU_WIDTH);
		    cableSlotOutline();
	       }
	       cableSlot();
	       screwHoles();
	  }
     }
}

$fs = 0.5;

intersection () {
//     translate ([115, -10, -1]) #cube ([50, 100, 100]);
     ac_cover ();

}
//translate ([200,0,0]) dc_cover ();

// power socket mockups

/* difference () { */
/*      cube ([5,45,50]); */
/*      #translate ([0, 0, -10]) powerSocket (); */
/* } */

/* translate ([100, 0, 0]) */
/* difference () { */
/*      cube ([5, 40,50]); */
/*      translate ([0, -30, -20]) #screwHoles(); */
/* } */
