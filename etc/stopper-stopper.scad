// Trival stopper for a drilled out homebrewing stopper

// Quick hack so I could store water in homebrewing carboys during
// hurricane florence

stem_base_d = 6;
stem_neck_d = 11;
stem_head_d = 14;
stem_handle_d = 20;

base_h = 8;
neck_h = 15;
top_h = 3;

$fa = 0.1;
$fs = 0.1;

hull () {
     cylinder (d=stem_base_d);
     translate ([0, 0, base_h]) cylinder (d=stem_neck_d);
}
hull () {
     translate ([0, 0, base_h]) cylinder (d=stem_neck_d);
     translate ([0, 0, base_h+neck_h]) cylinder (d=stem_head_d);
}
translate ([0, 0, base_h+neck_h]) cylinder (d=stem_handle_d, h=top_h);
