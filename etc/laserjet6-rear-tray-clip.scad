// Trivial clip to hold rear exit tray on my laserjet 6 in place
// (built in clips fatigued after 20 years). Includes an optional hook
// into the grille on the rear exit tray to secure it better.

// measurements:
// length = 73
// max depth = 25

// fixme: needs to be 72 I think
clip_gap    = 73.5;
clip_wall   = 4; // was 3, beefed up a bit after first pair broke
clip_depth  = 20;
clip_width  = 5;

hook        = true;
hook_depth  = 6;
hook_wall   = 3; // limited by size of rear output tray slots

$fs = 0.1;
$fa = 0.1;

linear_extrude (clip_width) {
     difference () {
	  square ([clip_gap + clip_wall * 2, clip_depth + clip_wall]);
	  translate ([clip_wall, -clip_wall]) square ([clip_gap, clip_depth + clip_wall]);
     }
     if (hook) {
	  translate ([clip_wall + clip_gap - hook_depth, 0]) square ([hook_depth, hook_wall]);
     }
}
