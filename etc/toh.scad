// Simple Toh press for incense trails
// 31mm long, 12mm wide

leg_length   = 31;
leg_width    = 12;
leg_depth    = 5;
leg_spacing  = 10;
handle_depth = 10;

cube([leg_width, leg_length, leg_depth]);

translate ([leg_spacing + leg_width,0,0]) cube([leg_width, leg_length, leg_depth]);

translate ([0, leg_length - leg_width, 0]) cube ([leg_spacing + leg_width * 2, leg_width, leg_depth + handle_depth]);
