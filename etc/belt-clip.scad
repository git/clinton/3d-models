// basic belt piece, intended for game of thrones costume originally

use <dotscad/rounded_square.scad> // https://github.com/JustinSDK/dotSCAD

$fs = 0.1;
$fa = 0.1;


buckle_height = 60;
buckle_width = 120;
buckle_thickness = 3;

slot_width = 3.5;
slot_height = buckle_height - 15;
slot_edge_offset = 5.5 + slot_width;

dragon_d = 90;

buckle_corner_r = 1.5;
slot_corner_r = 0.5;

module buckle_piece () {
     difference () {
	  rounded_square ([buckle_width/2, buckle_height], buckle_corner_r);

	  translate ([buckle_width/2 - slot_edge_offset, (buckle_height - slot_height) / 2, 0]) {
	       square ([slot_width, slot_height]);
	       translate ([slot_width/2, slot_height, 0]) circle (d=slot_width);
	       translate ([slot_width/2, 0, 0]) circle (d=slot_width);
	  }
     }
}

module buckle_base (buckle_thickness=buckle_thickness) {
     linear_extrude (buckle_thickness) {
	  translate ([-buckle_width/4, 0, 0]) square ([buckle_width/2, buckle_height]);
	  buckle_piece ();
	  mirror ([1, 0, 0]) buckle_piece ();
     }
}


// non-free dep: https://www.myminifactory.com/object/3d-print-game-of-thrones-house-targaryen-1285
module dragon (d=10) {
     translate ([-d/2, -d/2, 0]) {
	  resize ([d, 0, 0], auto=true ) import ("./house-targaryen-game-of-thrones-2_fixed.stl");
     }

     cylinder (d=d, h=buckle_thickness);
}


translate ([0, -buckle_height/2, 0]) buckle_base ();
//translate ([0, 0, 0]) dragon (d=dragon_d); // or whatever other geometry you want to integrate with belt
