// Foot for printdry filament dry to make it not rattle about
// Copyright (c) 2017 Clinton Ebadi <clinton@unknownlamer.org>

tolerance = 0.2;

width = 2.7 + tolerance;
length = 13.75 + tolerance;
height = 7.5 - 1.0 - tolerance;

wall = 2;
foot = 7;

$fa = 0.1;
$fs = 0.1;

module slot (length=length, width=width, height=height) {
     linear_extrude (height) {
	  hull () {
	       translate ([-(length - width)/2, 0, 0]) circle (d=width);
	       translate ([(length - width)/2, 0, 0]) circle (d=width);
	  }
     }
}

difference () {
     translate ([0, 0, -foot]) slot (length+wall, width+wall, height+foot-0.01);
     slot ();
}

