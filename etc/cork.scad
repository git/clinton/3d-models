// cork for my weep of willow potion bottle

// designed to fit a small submersible led tea light into the cap and
// illuminate the smoke rising from the smoker. LED sits in
// led_cork(), and led_cap() is placed over it to diffuse. Printed @
// 100% infill, slow, and with the largest possible layer heights.

$fa = 1;
$fs = 1;

led_cork ();
//led_cap ();

/* actual cork dimensions
bottom_d = 29.5;
top_d = 35.25;
height = 37.77;
*/

module cork (bottom_d, top_d, height) {
     hull () {
	  cylinder (d=bottom_d, h=0.1);
	  translate ([0, 0, height]) cylinder (d=top_d, h=0.1);
     }
}


// logically, should be arguments, but are shared between led_cork and
// led_cap...

bottom_inner_d = 30;
top_inner_d    = 35.25;
height         = 37.77;
wall           = 1.0;

module led_cork () {
     difference () {
	  cork (bottom_inner_d + wall*2, top_inner_d + wall*2, height);
	  translate ([0, 0, wall*2]) cork (bottom_inner_d, top_inner_d, height);
     }

}

// dome of the led, to be cut from cap
module led_dome (height = 8.5, diameter = 8.8) {
     cylinder (d=diameter, h=height/2);
     translate ([0, 0, height/2]) sphere (d=diameter);
     
}

module led_cap (cap_depth = 12, cap_tolerance = 0.5) {
     difference () {
	  // doesn't have to be exact, just need to fit in
	  cork (bottom_inner_d, top_inner_d - cap_tolerance, cap_depth);
	  translate ([0, 0, -0.01]) led_dome ();
     }
}
