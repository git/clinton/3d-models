// Simple cover for a mosquito bacillus thuringiensis trap

// UV radiation may slowly kill the bt... so use an opaque container
// and cover it with a lid with a smaller hole in it. Might help
// control evaporation as well, and it helps not having open
// containers of water around (probably don't want wild animals
// drinking a soup of decaying bug larvaue).

// The trap itself is just water, orange blossom water (which contains
// skatole... I think) as an attractant, and some bt granules or
// dunks.

$fa = 0.1;
$fs = 0.5;

module mosquito_honeypot_lid (lid_diameter = 50, lid_depth = 15, hole_width = 20, wall_thickness = 1, bottom_thickness = 2)
{
     total_h = lid_depth + bottom_thickness;
     difference () { 
	       cylinder (d = lid_diameter + wall_thickness*2, h = total_h, center = true);
	  translate ([0, 0, bottom_thickness/2 + 1/2])
	       #cylinder (d = lid_diameter, h = lid_depth+1, center = true);

	  translate ([0, 0, -lid_depth/2])
	       cube ([hole_width, hole_width, bottom_thickness*4], center = true);
     }
}

// wall thickness calculation halves value 
//mosquito_honeypot_lid (lid_diameter=72.5, lid_depth=18, wall_thickness=4, bottom_thickness=2);


mosquito_honeypot_lid (lid_diameter=71, lid_depth=25, wall_thickness=2, bottom_thickness=2, hole_width=25);
