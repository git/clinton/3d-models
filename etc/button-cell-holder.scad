// Simple button cell battery holder
// Created 2017 Clinton Ebadi <clinton@unknownlamer.org>

// Released under the https://wiki.creativecommons.org/wiki/CC0
// To the extent possible under law, Clinton Ebadi has waived all
// copyright and related or neighboring rights to simple button cell
// battery holder.

// Defaults have no tolerance, should be printed using flexible
// filament for a snug fit

module holder (battery_d = 20, battery_h = 3.2, case_wall = 1.7, slot_undersize = 0.4, wire_d = 1.5, cover_pct = 0.4) {
     battery_h = battery_h - slot_undersize;

     case_d = battery_d + case_wall*2;
     case_h = battery_h + case_wall*2;

     %cylinder (d= battery_d, h = battery_h, center = true);

     difference () {
	  cylinder (d = case_d, h = case_h, center = true);
	  cylinder (d= battery_d, h = battery_h, center = true);

	  // only cover cover_pct%
	  translate ([-case_d/2 + case_d*cover_pct, -case_d/2, -case_h/2 - 0.01]) cube ([case_d, case_d, case_h + 0.02]);

	  // slots for wires
	  hook_distance = -case_d/2 + case_d*cover_pct/2; // fixme: shitty name
	  for (z = [-battery_h/2, battery_h/2]) {
	       #translate ([-case_d/2 - 0.01, 0, z]) rotate ([0, 90, 0]) cylinder (h = case_d, d = wire_d);
	       //#translate ([-case_d/2 - 0.01, 0, z]) rotate ([0, 90, 0]) cylinder (h = case_d * cover_pct/2, d = wire_d);
	  }

	  // hole to bend wire up to hold in place
	  translate ([hook_distance, 0, 0]) cylinder (h = case_h + 1, d = wire_d, center = true);
     }
}

holder ($fs = 0.1, $fa = 1, slot_undersize = 0.4);
