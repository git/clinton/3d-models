// Super simple hook
// Intended for hanging a power strip from the top of a metal pegboard

// vertical and horizontal pieces

// actual measurements of board + power strip
/* v0_len = 47.5 - 10; */
/* v1_len = 47.5; */
/* v2_len = 42.5; */

/* h0_len = 26.75; */
/* h1_len = 35.25; */
/* h2_len = 35.25; */

v0_len = 47.5 - 10;
v1_len = 47.5;
v2_len = 42.5;

h0_len = 26.75;
h1_len = 10;
h2_len = 35.25;

thickness = 0.45 * 6; // four perimeters on prusa mini with 0.4mm nozzle
width     = 15;       // my power strip has a 20mm wide area where the hook could go
fillet    = 0;        // doesn't quite work, affects internal dimensions

$fa = 1;
$fs = 0.5;
linear_extrude (width) {
     offset (r = -fillet) offset (r = +fillet) union () {
	  square ([thickness, v0_len]); //v0
	  translate ([0, v0_len]) square ([h0_len + thickness * 2, thickness]); //h0
	  translate ([h0_len + thickness, v0_len - v1_len]) square ([thickness, v1_len]); // v1
	  translate ([h0_len + thickness, v0_len - v1_len - thickness]) square ([h2_len + thickness * 2, thickness]); //h2
	  translate ([h0_len + h2_len + thickness * 2, v0_len - v1_len]) square ([thickness, v2_len]); //v2
	  translate ([h0_len + h2_len + thickness * 2 - h1_len, v0_len - v1_len + v2_len]) square ([h1_len + thickness, thickness]); //h1
     }
}

