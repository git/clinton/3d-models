// super simple canister
// 2018 Clinton Ebadi

// incense sticks are ~22cm
// add a few cm in cap for packaging

inner_d = 25;
inner_h = 215;
cap_inner_h = 35; // height inside of cap, above rim of the canister
cap_overlap = 10;
cap_tolerance = 0.3;

wall = 2;
base = 2;
sides = 7;


// calculated
outer_d = inner_d + wall*2;
outer_h = inner_h + base;

cap_outer_d = outer_d + wall*2 + cap_tolerance;
cap_outer_h = cap_inner_h + cap_overlap + base;

cap_bump_size = 0.75;
cap_bump_h = 3;
cap_bump_offset = 3;

echo (cap_bump_offset);


// other asserts, if I care enough...
//assert (cap_inner_h > cap_overlap);
//assert (cap_outer_h - cap_bump_offset > base);


//$fa = 0.1;
//$fs = 0.1;

canister_base ();
translate ([cap_outer_d + 1, 0, 0]) canister_cap ();
//#translate ([0, 0, outer_h + cap_inner_h + base]) rotate ([0, 180, 180]) canister_cap ();

//cap_bump ();

module cap_bump (cap_bump_size=cap_bump_size, cap_bump_h=cap_bump_h) {
     cap_bump_d = outer_d+cap_bump_size/2;
     hull () {
	  translate ([0, 0, 0])
	       cylinder (d = cap_bump_d, h = cap_bump_h/3, $fn = sides);
	  translate ([0, 0, cap_bump_h/3])
	       cylinder (d = cap_bump_d + cap_bump_size, h = cap_bump_h/3, $fn = sides);
	  translate ([0, 0, cap_bump_h/3*2])
	       cylinder (d = cap_bump_d, h = cap_bump_h/3, $fn = sides);
     }
}

module canister_base () {
     difference () {
	  union () {
	       cylinder (d = outer_d, h = outer_h, $fn = sides);
	       translate ([0, 0, outer_h - cap_overlap + cap_bump_offset]) cap_bump ();
	       translate ([0, 0, outer_h - cap_overlap - cap_bump_h - 0.2]) cap_bump (cap_bump_size = wall);
	  }
	  translate ([0, 0, base]) cylinder (d = inner_d, h = outer_h + 1, $fn = sides);
     }
}

module canister_cap () {
     tolerance_pct = 1.00;
     difference () {
	  cylinder (d = cap_outer_d, h = cap_outer_h, $fn = sides);
	  translate ([0, 0, base]) cylinder (d = outer_d + cap_tolerance, h = cap_outer_h+1, $fn = sides);
	  translate ([0, 0, cap_outer_h - cap_bump_offset - cap_bump_h]) scale ([tolerance_pct, tolerance_pct, tolerance_pct]) rotate ([0, 0, 0]) cap_bump ();
     }
}
