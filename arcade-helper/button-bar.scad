/* Arcade Button Bar Helper
 * Copyright (C) 2017  Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// todo
//  - integrate good key cap library for example keys


$fs = 0.1;

cols = 6;
rows = 1;

bolt_depth = 12;
screw_d = 3.5;

wall = 4;
bottom = 2;
border = 3;

nutcatch_size = 6;


// switch depth + pin depth + clearance + room for mcu and wires
case_depth = 3.5 + 3.3 + 3.2 + 3;

echo ("case_depth = ", case_depth);

button_size = 20;

// derived
box_size_x = cols*button_size+border*2;
box_size_y = rows*button_size+border*2;


module supports () {
     for (x = [ 0, ceil (cols/2)*button_size+border*2 - nutcatch_size, box_size_x - nutcatch_size], y = [ 0, box_size_y - nutcatch_size]) {
	  translate ([x, y, 0]) {
	       children ();
	  }
     }
}

module plate_hole () {
     translate ([nutcatch_size/2, nutcatch_size/2, 0])
	  circle (d = screw_d, $fs = 0.1);
}

module nutcatch (screw_only=false) {
	  translate ([nutcatch_size/2, nutcatch_size/2, (case_depth+wall)/2]) {
	       difference () {
		    if (!screw_only) cube ([nutcatch_size, nutcatch_size, case_depth+wall], center=true);

		    translate ([0, 0, (case_depth+wall-bolt_depth)/2+0.01]) cylinder (d = screw_d - 1, h = bolt_depth, center=true, $fs=0.1);
	       }
	  }
}

module cherrymx_slot () {
     width = 14;
     square (width, center=true);
     // y=width-2 is supposed to be correct, but seemed better this way
     square ([width+2, width-3], center=true);
}


module switch_plate_base (bevel=2, holes=true) {
     difference () {
	  offset (r=bevel) offset (r=-bevel) square ([box_size_x, box_size_y]);
	  translate ([border, border, 0]) {
	       for (i = [ 0 : cols-1 ]) {
		    for (j = [ 0 : rows-1 ]) {
			 translate ([i*button_size + button_size/2, button_size/2 + j*button_size, 0])
			      children ();
		    }
	       }
	  }
	  if (holes) {
	       supports () plate_hole ();
	  }
     }
}




module switch_plate (thickness=1.5, bevel=2) {
     linear_extrude (height=thickness) {
	  switch_plate_base (cols=cols, rows=rows, button_size=button_size, bevel=bevel)
	  {
	       children ();
	  }
     }
}



module case (depth=case_depth, wall=wall) {
     module usb_port (w = 13, h = bottom + 6) {
	  translate ([wall, box_size_y/2-w/2, bottom]) rotate ([0, 0, 90])  cube ([w, wall+0.01, h]);

     }

     module mcu_holder () {
	  barrier_depth = 3;
	  barrier_x = 35.0 + nutcatch_size;
	  barrier_width = 2;

	  translate ([barrier_x, 0, bottom]) cube ([barrier_width, box_size_y, barrier_depth]);
     }

     difference () {
	  linear_extrude (height=depth+bottom) switch_plate_base (cols=cols, rows=rows, holes=false);
	  supports () nutcatch (screw_only=true);

	  difference () {
	       translate ([wall/2, wall/2, bottom+0.01])
		    linear_extrude (height=depth) resize ([box_size_x-wall, box_size_y-wall, 1]) switch_plate_base (cols=cols, rows=rows, holes=false);
	       supports () nutcatch ();
	  }
	  usb_port ();
     }
     mcu_holder ();
}

module keys () {
     $fs = 0.1;
     $fa = 0.1;
     keys = [ "", "",  "",  "" ,"",];

     for (i = [ 0 : len (keys) ])
	  translate ([i * button_size + border, border, 0])
	       oneAlphanumericKey(keys[i], xOffset=-38.5, font="fontawesome");

     translate ([(cols-1) * button_size + border, (rows-1) * button_size + border, 0]) oneAlphanumericKey ("⏎", xOffset=-38.5, font="DejaVu Sans");
}

module test_fit () {
     color ("OliveDrab") switch_plate () { cherrymx_slot (); }
     color (alpha=0.6) translate ([45, 10, 10]) keys ();
     translate ([0, 0, -30]) case ();

}

module print () {
     switch_plate () { cherrymx_slot (); }
     translate ([0, box_size_y+5, 0]) case ();
}

print ();
//test_fit ();

/* module keys () { */
/*      $fs = 0.1; */
/*      $fa = 0.1; */
/*      keys = [  "", "", "",  "", "" ]; */
/*      for (j = [ 0 : rows-1 ]) { */
/* 	  for (i = [ 0 : cols-1 ]) { */
/* 	       index = i*(cols-1)+j; */
/* 	       if (index < len(keys)) { */
/* 		    echo (i, j, i*j, index); */
/* 		    translate ([i * button_size + border, j * button_size + border, 0]) */
/* 			 oneAlphanumericKey(keys[index], xOffset=-38.5, font="fontawesome"); */
/* 	       } */
/* 	  } */
/*      } */


/*      translate ([(cols-1) * button_size + border, (rows-1) * button_size + border, 0]) oneAlphanumericKey ("⏎", xOffset=-38.5, font="DejaVu Sans"); */
/* } */
