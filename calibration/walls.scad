// empty cube to test ninjaflex inner/outer wall adhesion

nozzle_diameter = 0.5;
d = 20;
h = 10;

wall = nozzle_diameter*2;
base = 1;


difference () {
     cube ([d, d, h], center=true);
     translate ([0, 0, base]) #cube ([d-wall*2, d-wall*2, h+0.01], center=true);
}
     
