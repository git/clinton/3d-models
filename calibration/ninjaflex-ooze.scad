// quick hack to test ooze with ninjaflex

// lots of small towerslike in the make magazine tests are doomed with
// ninjaflex, so just use four larger towers

tower_base_d  = 10;
tower_top_d   = 2;

tower_height  = 20;
tower_spacing = 15;

base_height  = 0.8;
base_extra_d = 0;

$fn = 4;


module tower () {
     cylinder (d1=tower_base_d, d2=tower_top_d, h=tower_height);
}

module towers () {
     spacing = tower_spacing + tower_base_d;
     
     for (x = [0, spacing], y = [0, spacing]) {
	  translate ([x, y, 0]) tower ();
     }

     translate ([spacing/2, spacing/2, 0]) tower ();
}

difference () {
     linear_extrude (height = base_height) offset (r = base_extra_d/2, chamfer=true) hull () projection () towers ();
     translate ([0, 0, 0]) cylinder (d=tower_base_d/2, h=base_height/2);
}
translate ([0, 0, base_height]) towers ();


