// quick hack to test linear pressure advance with a side of mild
// retraction torture

d = 30;

translate ([d,0,0]) cube (d);

translate ([-d-10,0,0]) cube ([d/3, d, d]);

translate ([-d-10,-d*2,0]) cube (d/2);
translate ([d+15,-d*2,0]) cube (d/4);

/*

 
try 2mm retract (then 35mm/s?)
2mm jammed
35mm/s jammed even with R2000 and 5.0 ejerk

M905 K50
M204 T2000 P500 R3000
M205 X8.0

M905 K50
M204 T2000 P500 R2000
M205 X12.0 E5.0

M905 K50
M204 T2000 P500 R3000
M205 X12.0 E5.0

next bump ejerk if needed
R3000 = boom

M905 K50
M204 T2000 P500 R2000
M205 X12.0 E10.0

everything fucking sucks
M905 K50
M204 T2000 P500 R2000
M205 X12.0 E5.0

seriously
M905 K0
M204 T2000 P500 R3000
M205 X12.0 E10.0

round 2:

 - ooze test 35mm/s 1.5mm retract with solid green and K50. Fail or
   succeed does not matter, just need a reference for "it can work" or
   "fail expected"
 - several ooze test models with K0 and atomic filament
   - Try R2000, 25mm/s 1.5mm, E10.0. Reduce jerk, then accel if needed
   - Try R3000 (if it works), 35mm/s 1.5mm
   - Intermediate retract speeds if required

   initial result: both did ooze test @ max. plain green filament does
   robot at most brutal 1.5mm settings without issues...

M203 E35.0
M204 T2000 P500 R3000
M205 X15.0 E10.0
M905 K50



M203 E35.0
M204 T2000 P500 R3000
M205 X12.0 E10.0
M905 K50

*/
