/*
  Simple model to test for surface ringing at high print speeds.
 */

$fn = 64;

radius = 35;
height = 35;
n_pits = 3;

pit_radius = height/n_pits/2;
//pit_height = radius*2;

difference () {
     cylinder (h = height, r = radius, center = true, $fn = 128);
     cylinder (h = height+1, r = radius-radius/4, center = true, $fn = 128);
     pits (offset = radius);
}
pits (offset = -radius);

 pit_rotations = rands (min_value = 0, max_value = 180, value_count = n_pits);

module pits (offset) {
     for (i = [0 : n_pits - 1]) {
	  echo (pit_rotations[i]);
	  rotate ([0, 0, pit_rotations[i]])
	  translate ([offset, 0, -height/2+pit_radius+i*height/n_pits]) 
	       rotate ([0, 90, 0]) {
	       /*translate ([0, 0, -3])*/ sphere (r = pit_radius);
//	       cylinder (h = 6, r = pit_radius, center = true);
	       }
     }
}


module random_balls () {
     pit_offsets = rands (min_value = -radius/2, max_value = radius/2, value_count = n_pits);
     pit_rotations = rands (min_value = 0, max_value = 180, value_count = n_pits);

     for (i = [0 : n_pits-1]) {
	  translate ([0, 0, max (-pit_radius, i*height/n_pits-pit_height)]) rotate ([pit_rotations[i], 90, 0])
	       translate ([radius, radius, 0]) sphere (r = pit_radius);
	  //cylinder (h = pit_height, r = pit_radius, center = true);
     }
}
