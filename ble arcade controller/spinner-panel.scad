// simple spinner control for a 5 3/8 inch by 8 1/2 inch project box
// from the craft store.


panel_height  = (5 + 1/2) * 25.4;
panel_width = (8 + 5/8) * 25.4;
wall_width   = 7.5;



button_d = (1 + 1/8) * 25.4;
button_spacing = 36; // based on https://www.slagcoin.com/joystick/layout.html
$button_d = button_d;

module line (pos0, pos1, width=0.5) {
     hull () {
	  translate (pos0) circle (width);
	  translate (pos1) circle (width);
     }
}

module button (bezel = $button_d+4, lines=true) {
     if (lines) {
	  line ([-bezel/2, 0], [bezel/2, 0]);
	  line ([0, -bezel/2], [0, bezel/2]);
     }
//     line ();
     difference () {
	  circle (d=$button_d);
	  circle (d=$button_d-2);
     }
     circle (d = $button_d/4);
     %circle(d=bezel);
}

module panel_layout () {
     // buttons
     translate ([panel_width/5, panel_height/2]) {
	  translate ([0, 0]) button ();
	  translate ([button_spacing, -button_spacing/2]) button ();
     }

     // spinner
     translate ([panel_width-panel_width/5, panel_height-panel_height/4]) {
	  translate ([0, 0]) button ();
     }
     

}

module panel_outline () {
     difference () {
	  square ([panel_width, panel_height]);
	  translate ([wall_width/2, wall_width/2]) square ([panel_width-wall_width, panel_height-wall_width]);
     }
}

union () {
     panel_outline ();
     panel_layout ();
}



// centering aid for drilling pilot hole

module template_button (bezel = $button_d+4) {
     difference () {
     circle (d=$button_d);
     circle (d = 3);
     }

}

//linear_extrude (3) template_button ($fa=0.1, $fs=0.1);
