// Simple openscad program to split the creepy hand so larger creepy
// hands can be made, or just so the hand can be removed to make it
// easier to put things on the hand.

// Copyright (c) 2019 Clinton Ebadi <clinton@unknownlamer.org>, GPLv3
// or at your option any later version.

// Can be used to split other things: just load whatever geometry you
// want and ensure the spot you want to split it at is centered on the
// origin.

// TODO:
// - allow setting a z offset percentage to shift holes more into upper
//   or lower piece

// Creepy hand from http://www.thingiverse.com/thing:8174 by whosawhatsis, used with permission (CC BY)
// Modified by centering it on the origin (no actual changes)
// Defaults are for #10 threaded rod (hardware store doesn't sell metric rod here)
split_thing (part_spacing = 100) {
      scale ([model_scale, model_scale, model_scale]) translate ([-2, 3, 0]) rotate ([0, 0, 40]) translate ([0, 0, 10]) import ("hand-centered.stl");
}

default_rod_d = 5.1; // loose fit on #10 threaded rod
default_depth = 20;
default_spacing = 25;
max_model_dim = 1000;
model_scale = 2;

// [Hidden]
$fa = 1;
$fs = 1;

// place child object, will be split on plane at z = 0
module split_thing (top = true, bottom = true, rod_d = default_rod_d, depth = default_depth, spacing = default_spacing, part_spacing = 50) {
     module rod_mount () {
	  module rod () {
	       translate ([-spacing/2, 0, 0]) cylinder (d=rod_d, h=depth);
	       translate ([spacing/2, 0, 0]) cylinder (d=rod_d, h=depth);
	       translate ([0, -spacing/4, 0]) cylinder (d=rod_d, h=depth);
	       translate ([0, spacing/4, 0]) cylinder (d=rod_d, h=depth);
	  }
	  
	  rod ();
	  mirror ([0, 0, 1]) rod();
     }
     
     module whole () {
	  difference () {
	       union () { children (); }
	       rod_mount ();
	  }
     }

     if (top) {
	  intersection () {
	       whole () children ();
	       translate ([-max_model_dim/2,-max_model_dim/2, 0]) cube (max_model_dim);
	  }
     }
     if (bottom) {
	  translate ([top ? part_spacing : 0, 0, 0]) {
	       rotate ([180, 0, 0]) {
		    intersection () {
			 rotate ([0, 0, 0]) whole () children ();
			 mirror ([0, 0, 1]) translate ([-max_model_dim/2,-max_model_dim/2, 0]) cube (max_model_dim);
		    }
	       }
	  }
     }

}
