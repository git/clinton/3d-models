/*
min y = 0
max y = 100
min x = 1205
max x = 1254

Makerfair X carriage plate adapter for the Wilson RepRap.
Copyright (c) 2015 Clinton Ebadi <clinton@unknownlamer.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Measurements reverse engineered from
https://github.com/RickSisco/MakerFarm-Prusa-i3v-12-Inch

Intended for use with the itty bitty dual extruder series.

Source needs a good cleanup. Evil loops that don't take advantage of
the geometry of the object abound.

major todo:

 - square hole placement is a total mess

*/

use <MCAD/nuts_and_bolts.scad>

$fn = 64;

hole_tolerance = 0.3; // widen holes a bit, may still need to drill out

// Size of mounting bolt heads so they can be recessed
bolthead_depth = 2;
bolthead_diameter = 5.5 + hole_tolerance;

// X-Carriage plate size

// Wilson II
// WARNING:  not yet compatible with rack and pinion carriage
carriage_height = 72.35;
carriage_width = 60;
shelf_offset = [0, 3, 0]; // shift the mount for extruder

// Wilson TS
//carriage_height = 64;
//carriage_width = 51;
//shelf_offset = [0, 0, 0]; // shift the mount for extruder

plate_width = 75; // wilson carriage is only 50mm wide!
plate_depth = 6 + bolthead_depth; // wilson default depth = 8, makerfarm = 6

plate_cut_height = 6.5;
plate_cut_width = 6;
plate_cut_distance = 13.3;
plate_cut_y_offset = 27;
plate_cut_bolt = 3 + hole_tolerance;
plate_cut_x_bolt_offset = plate_cut_distance + plate_cut_bolt / 2;

plate_extra_height = 6; // make plate a bit taller so it is structurally sound

plate_height = plate_cut_height + plate_cut_y_offset + plate_extra_height;

lower_cut_y_offset = 5.33; // 5.325 in CAD
lower_bolt_x_offset = 1.5 + plate_cut_bolt / 2;
lower_bolt_y_offset = 3.275 + plate_cut_bolt / 2;

carriage_bolt = 4 + hole_tolerance;
carriage_hole_spacing = 23;
carriage_clearance = 20; // holes are 20mm from bottom of plate
carriage_y_offset =  plate_cut_y_offset + plate_cut_height + carriage_clearance;
carriage_nut_depth = 3.2 + 3.2/2 + 0.1; // m4 nut depth + half of an m4 nut + tolerance
carriage_offset = [(plate_width - carriage_width) / 2, 0 , 0];

corner_radius = 3; // round off the corners to make it look nicer

module itty_wilson () {
     difference () {
	  union () {
	       translate (carriage_offset) carriage_mount_base ();
	       translate (shelf_offset) shelf_mount_base ();
	  }
	  rotate ([0, 0, -1]) translate (carriage_offset) carriage_mount_holes ();
	  translate (shelf_offset) shelf_mount_holes ();
     }
}

itty_wilson ();

// mount to wilson x-carriage
module carriage_mount_base () {
     linear_extrude (height = plate_depth, convexity = 8) {
	  // outline of plate + bolt holes
	  translate ([corner_radius, corner_radius, 0]) {
	       minkowski () {
		    square ([carriage_width - corner_radius*2, carriage_height - corner_radius*2]);
		    circle (r = corner_radius);
	       }
	  }
     }
}

module carriage_mount_holes () {
     #translate ([(carriage_width - carriage_hole_spacing) / 2, (carriage_height - carriage_hole_spacing) /2, 0])
     for (x = [0, carriage_hole_spacing], y = [0, carriage_hole_spacing]) {
	  translate ([x, y, -50.1]) cylinder (d = carriage_bolt, h = plate_depth+100);
     }

     // after extruding, cut out spaces for M4 hex nuts.
     translate ([(carriage_width - carriage_hole_spacing) / 2, (carriage_height - carriage_hole_spacing) /2, plate_depth - carriage_nut_depth]) {
	  for (x = [0, carriage_hole_spacing], y = [0, carriage_hole_spacing]) {
	       translate ([x, y, 0]) linear_extrude (height = carriage_nut_depth + 0.01) nutHole (size = carriage_bolt, proj = 1);
	  }
     }
}

// mount for makerfarm extruder shelf
module shelf_mount_base () {
     linear_extrude (height = plate_depth, convexity = 8) {
	  translate ([corner_radius, corner_radius, 0]) {
	       minkowski () {
		    square ([plate_width - corner_radius*2, plate_height - corner_radius*2]);
		    circle (r = corner_radius);
	       }
	  }
     }
}

module shelf_mount_holes () {
     depth = plate_depth*2+0.4;
     translate ([0, 0, -0.1]) {
	  // upper shelf mount
	  translate ([plate_cut_width/2, plate_cut_y_offset, 0]) {
	       for (x = [-0.001, plate_cut_width + plate_cut_distance,
			      plate_width - plate_cut_width + 0.001, plate_width - plate_cut_width*2 - plate_cut_distance])
			 translate ([x, plate_cut_height/2, 0]) cube ([plate_cut_width + hole_tolerance, plate_cut_height, depth], center = true);

			 // looks like the bolt is at the midpoint between the cut outs
			 for (x = [plate_cut_x_bolt_offset - plate_cut_width, plate_width - plate_cut_x_bolt_offset])
			      translate ([x, plate_cut_height / 2, 0]) {
				   #cylinder (d = plate_cut_bolt, h = depth);
				   cylinder (d = bolthead_diameter, h = bolthead_depth+0.1);
			      }

	       }

	       // lower shelf mount
	       translate ([0, lower_cut_y_offset, 0]) {
		    for (x = [plate_cut_width/2 - 0.01, plate_width - plate_cut_width/2 + 0.01]) {
			 // lower cut out is closer to 6mm than 6.5mm in cad drawings
			 translate ([x, plate_cut_height/2, 0]) cube ([plate_cut_width, plate_cut_height - 0.5, depth*2], center=true);
	       }
		    // I think the intention is for the screw holes to be centered over the cut out
		    for (x = [lower_bolt_x_offset, plate_width - lower_bolt_x_offset])
			 translate ([x, plate_cut_height + lower_bolt_y_offset, 0]) {
			      cylinder (d = plate_cut_bolt, h = depth);
			      cylinder (d = bolthead_diameter, h = bolthead_depth+0.1);
			 }
	       }
	  }
}
