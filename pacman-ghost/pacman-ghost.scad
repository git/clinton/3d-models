//
// Pacman Ghost
// ------------
//
// Created by Joao Alves (jpralves@gmail.com)
// Tea Light added by Clinton Ebadi <clinton@unknownlamer.org>
// Pac Guy model by Fry Hyde
// ------------------------------------------
//
// Parameters:

// Figure diameter
ghost_diameter = 50; // [25:5:100]
pacguy_diameter = ghost_diameter * 1.5;

height = ghost_diameter*0.65;
// Add keychain
key_chain = 0; // [1:On, 0:Off]
// Add LED hole
led_hole = 0; // [0:Off, 5:5mm, 8:8mm, 10:10mm]
// Add tea light holder
tea_light = true;

// Render Ghost
ghost = true;
// Render Pac Guy
pacguy = false;

// Detail
$fa=4; // [20:Low Res, 10:Normal Res, 4:Hi Res]

// [Tea Light]
base_diameter = 30;
base_height   = 16.2;
bulb_diameter = 9;
// Built in sacrificial support layer
tea_light_support = true;
tea_light_support_thickness = 0.3;

/* [Hidden] */
$fs=0.1;

module ghost() {
     difference () {
	  union() {
	       cylinder(d = ghost_diameter, h = height);
	       translate([0, 0, height]) sphere(d = ghost_diameter);
	       if (key_chain == 1) {
		    translate([0, 0, height+ghost_diameter/2])
			 rotate([0, 90, 0])
			 rotate_extrude()
			 translate([4, 0, 0])
			 circle(r = 1.5);
	       }
	  }
	  translate([ghost_diameter/2, -ghost_diameter/4, height])
	       sphere(d = ghost_diameter/3.5);
	  translate([ghost_diameter/2, ghost_diameter/4, height])
	       sphere(d = ghost_diameter/3.5);
	  translate([0, 0, -0.1])
	       for(a = [0:11]) {
		    rotate([a/12*360,-90,0])
			 scale([1.5,1,1])
			 translate([0,0,ghost_diameter/12.5])
			 cylinder(ghost_diameter, 0,ghost_diameter/25*6.5, $fn=4);
	       }
     }
}

module led_hole (led_d = led_hole, led_h = 0) {
     // led_h==0 auto-sets height, openscad can't reference other
     // arguments so this abomination arises
     led_h = led_h ? led_h : height - (led_d+2);

     union() {
	  cylinder(d = led_d+2, h = led_h);
	  translate([0, 0, led_h]) sphere(d = led_d+2);
     }
}

module tea_light_base () {
     difference () {
	  union () {
	       cylinder (d = base_diameter + 1, h = base_height + 1);
	       led_hole (led_d = bulb_diameter, led_h = base_height+8);
	  }
	  // sacrificial support layer, subtracted since this will be
	  // subtracted from the ghost body
	  if (tea_light_support) {
	       translate ([0, 0, base_height+(1 - tea_light_support_thickness)])
		    #cylinder (d=base_diameter+2, h=tea_light_support_thickness);
	  }
     }
}

module pacguy () {
     // original model ghost_diameter is 30.464mm, scale to 1mm and then to
     // final ghost_diameter. z is slightly smaller due to bottom being cut
     // flat.
     s = 1 / 30.464 * pacguy_diameter;
     scale ([s, s, s]) translate ([0, 0, 28.742 / 2]) import ("PacGuy.stl", convexity=10);
}

module subtract_led () {
     difference () {
	  children ();
	  if (led_hole > 0) led_hole ();
	  if (tea_light) tea_light_base ();
     }
}
if (ghost) {
     subtract_led () {
	  rotate ([0, 0, -90]) ghost ();
     }
}

if (pacguy) {
     translate ([ ghost ? (ghost_diameter + pacguy_diameter) / 2 + 10 : 0, 0, 0]) subtract_led () {
	  pacguy ();
     }
}
