// Simple 3D Printable Drinking Horn
// Copyright (c) 2021 Clinton Ebadi <clinton@unknownlamer.org>


// Released under the https://wiki.creativecommons.org/wiki/CC0
// To the extent possible under law, Clinton Ebadi has waived all
// copyright and related or neighboring rights to Simple 3D Printable Drinking Horn.

// Single person mead horn, for winter solstice 2021 since sharing
// mead from the real horn is probably still unsafe.

use <dotscad/shape_circle.scad>;
use <dotscad/helix_extrude.scad>;

bottom_radius =   4;
top_radius    =  33;
horn_length   = 135;
horn_rise     =  40;
horn_twist    =   0;
horn_aspect   = [ 1.0, 0.92 ]; // [0:0.01:100]

module helix_horn () {
     helix_levels = 1/6;

     arc_measure  = (360 * helix_levels) * (3.13 / 180);
     arc_length   = horn_length;
     helix_radius = (arc_length / arc_measure);

     // Round off end
     // todo: dotscad rounded cone instead?
     translate ([helix_radius, 0, 0]) scale ([horn_aspect[0], 1/2, horn_aspect[1]]) intersection () {
	  sphere (r = bottom_radius);
	  rotate ([90, 0, 0]) cylinder (h = bottom_radius, r = bottom_radius, center=false);
     }

     shape_pts = [ for (pts = shape_circle (radius = bottom_radius)) [ pts[0] * horn_aspect[0], pts[1] * horn_aspect[1] ] ];

     helix_extrude(shape_pts,
		   radius = helix_radius,
		   scale = top_radius/bottom_radius,
		   levels = helix_levels,
		   level_dist = horn_rise / helix_levels,
		   vt_dir = "SPI_UP",
		   twist = horn_twist / helix_levels
	  );
}

helix_horn ($fs = 0.1, $fa = 1);
