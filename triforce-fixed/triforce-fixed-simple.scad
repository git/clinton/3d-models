// Fix for http://www.thingiverse.com/thing:24714
// Copyright (c) 2015 Clinton Ebadi <clinton@unknownlamer.org>
// CC Attribution 4.0 http://www.thingiverse.com/thing:24714

// Trifull.STL is the original model, 
// "Crest of Hyrule Mantlepiece by Mr_MegaTronic
// Published on June 10, 2012
// www.thingiverse.com/thing:24714
// Creative Commons - Attribution"

// Left leg is malformed, just use the good side twice

module trihalf () {
	difference () {
		translate ([0, 3.5, 0]) import("trifull.STL");
		translate([-33, -70, -2])  #cube (70);
	}
}


trihalf();
// openscad says these can't touch which is stupid, 
// insert a teensy tiny gap to make it export
translate([0,-0.00001,0]) mirror ([0,1,0]) trihalf ();
