#Name: Dual Temp Test
#Info: Lower the temperature for the nozzle that is not printing
#Depend: GCode
#Type: postprocess
#Param: coolTemperatureDifference(float:50.0) Drop idle nozzle temperature (C)
#Param: heatupTimeDelay(float:20.0) Heatup nozzle before use (sec)
#Param: zHop(float:0.0) Raise Z when reheating active extruder (mm)


__copyright__ = "Copyright (C) 2013 David Braam, Copyright (C) 2016 Clinton Ebadi <clinton@unknownlamer.org> - Released under terms of the AGPLv3 License"
import re
import math
from Cura.util import profile

def getValue(line, key, default = None):
	if not key in line or (';' in line and line.find(key) > line.find(';')):
		return default
	subPart = line[line.find(key) + 1:]
	m = re.search('^[0-9]+\.?[0-9]*', subPart)
	if m is None:
		return default
	try:
		return float(m.group(0))
	except:
		return default

def wipeNozzle (f):
        f.write ("""
G91
G0 X{d} Y{d} F{rate}
G0 X-{d} Y-{d} F{rate}
G0 X-{d} Y-{d} F{rate}
G0 X{d} Y{d} F{rate}
G90
""".format (d=3, rate=float(profile.getProfileSetting('travel_speed')) * 60))

fanOnCommand = None

def disableFan(f):
        if fanOnCommand is not None:
                f.write ("M107\n")

def enableFan(f):
        if fanOnCommand is not None:
                # ensure fan actually kicks back on
                f.write ("M106 S255\n")
                f.write (fanOnCommand)

with open(filename, "r") as f:
	lines = f.readlines()

z = 0.
x = 0.
y = 0.
e = 0.
feedrate = float(profile.getProfileSetting('print_speed'))
wipeTowerP = profile.getProfileSetting('wipe_tower') == 'True'
currentSectionType = 'CUSTOM'
activeExtruder = -1
firstLayer = True
# todo: use 2nd/3rd/etc extruder temp if available (arrays are alright...)
printTemperature = float(profile.getProfileSetting('print_temperature'))
coolTemperature = printTemperature - coolTemperatureDifference

with open(filename, "w") as f:
        # skip start gcode
	idx = lines.index (';LAYER:0\n')
        startIdx = idx
        for line in lines[0:idx]:
                if getValue(line, 'T', None) is not None and getValue(line, 'M', None) is None:
	                activeExtruder = getValue(line, 'T', None)
                f.write (line)
        while idx < len(lines):
		line = lines[idx]
		if getValue(line, 'T', None) is not None and getValue(line, 'M', None) is None:
			nextExtruder = getValue(line, 'T', None)
			printTime = 0.0
			sx = x
			sy = y
			sz = z
			for n in xrange(startIdx, idx):
				line = lines[n]
				if line.startswith(';'):
					continue
				g = getValue(line, 'G', None)
				if g == 0 or g == 1:
					nx = getValue(line, 'X', x)
					ny = getValue(line, 'Y', y)
					nz = getValue(line, 'Z', z)
					feedrate = getValue(line, 'F', feedrate)
					printTime += math.sqrt((nx-x)*(nx-x)+(ny-y)*(ny-y)+(nz-z)*(nz-z)) / feedrate * 60
					x = nx
					y = ny
					z = nz

			heatupTime = printTime - heatupTimeDelay
                        if heatupTime < 0.0:
                                heatupTime = printTime / 2.0 #None
                        heatupBlocked = not firstLayer # do not reheat until active extruder is heated
			x = sx
			y = sy
			z = sz

			f.write("M104 T%d S%d\n" % (nextExtruder, coolTemperature))
			f.write("T%d\n" % (activeExtruder))

			printTime = 0.0
			for n in xrange(startIdx, idx):
				line = lines[n]
				if line.startswith(';'):
					f.write(line)
					continue
                                if getValue(line, 'M', 666) == 107:
                                        fanOnCommand = None
                                        f.write(line)
                                        continue
                                if getValue(line, 'M', 666) == 106:
                                        fanOnCommand = line
                                        f.write(line)
                                        continue
                                # find where the newly activated extruder is unspooled and block until fully reheated
                                if (getValue(line, 'G', 666) == 0 or getValue(line, 'G', 666) == 1) and getValue (line, 'E', 666) == 0.0 and (heatupBlocked or firstLayer):
                                        # todo: if wipeTowerP, only
                                        # wait for a few degrees below
                                        # printTemperature and allow
                                        # it to finish heating while
                                        # priming
                                        if zHop > 0.0:
                                                f.write ("G91\nG0 Z%.3f\nG90\n" % zHop)

                                        disableFan(f)
                                        f.write("M109 T%d S%d\n" % (activeExtruder, printTemperature))
                                        wipeNozzle(f)
                                        enableFan(f)

                                        if zHop > 0.0:
                                                f.write ("G91\nG0 Z%.3f\nG90\n" % -zHop)
                                                zHop = -1
                                        heatupBlocked = False
				if heatupTime is not None:
					g = getValue(line, 'G', None)
					if g == 0 or g == 1:
						nx = getValue(line, 'X', x)
						ny = getValue(line, 'Y', y)
						nz = getValue(line, 'Z', z)
						feedrate = getValue(line, 'F', feedrate)
						printTime += math.sqrt((nx-x)*(nx-x)+(ny-y)*(ny-y)+(nz-z)*(nz-z)) / feedrate * 60
						if (printTime > heatupTime) and (not heatupBlocked):
							f.write("M104 T%d S%d\n" % (nextExtruder, printTemperature))
							f.write("T%d\n" % (activeExtruder))
							heatupTime = None
						x = nx
						y = ny
						z = nz
				f.write(line)
			f.write(lines[idx])
                        startIdx = idx + 1
			activeExtruder = nextExtruder
                        firstLayer = False
		idx += 1
	for n in xrange(startIdx, idx):
		f.write(lines[n])
